function mage(name, type, gender, exp, avatar){
	player.apply(this, [name,type,gender,exp,avatar]);
	
	this.fire_skills = {
	    name : 'fire_skills',
		fireball: false,
		firebolt: false,
		heatstorm: false,
		description: '',
		
		set_skills : function(fireball, firebolt, heatstorm) {
			this.fireball = fireball;
			this.firebolt = firebolt;
			this.heatstorm = heatstorm;
		},
		
		get_skills : function(){
			return this;
		}
	}
	this.ice_skills = {
		name : 'ice_skills',
		description: '',
		
		set_skills : function(){
			
		},
		
		get_skills : function(){
			return this;
		}
	}
	
	this.hybrid_skills = {
		description: '',
		
		set_skills : function(){
			
		},
		
		get_skills : function(){
			return this;
		}
	}
}


function fighter(name, type, gender, exp, avatar){
	player.apply(this, [name,type,gender,exp,avatar]);
	
	this.offensive_skills = {
        name : 'offensive_skills',
		description: '',
		
		set_skills : function() {

		},
		
		get_skills : function(){
			return this;
		}
	}
	this.defensive_skills = {
		name : 'defensive_skills',
		description: '',
		
		set_skills : function(){
			
		},
		
		get_skills : function(){
			return this;
		}
	}
	
	this.hybrid_skills = {
		description: '',
		
		set_skills : function(){
			
		},
		
		get_skills : function(){
			return this;
		}
	}
}


function archer(name, type, gender, exp, avatar){
	player.apply(this, [name,type,gender,exp,avatar]);
	
	this.elemental_skills = {
        name : 'elemental_skills',
        description: '',
        
		set_skills : function() {

		},
		
		get_skills : function(){
			return this;
		}
	}
	this.martial_skills = {
        name : 'martial_skills',
        description: '',
        
		set_skills : function(){
			
		},
		
		get_skills : function(){
			return this;
		}
	}
	
	this.hybrid_skills = {
		description: '',
		
		set_skills : function(){
			
		},
		
		get_skills : function(){
			return this;
		}
	}
}

function custom_class(name,type,gender,exp,avatar,skill_tree_1,skill_tree_2,skill_tree_3){
	player.apply(this,[name,type,gender,exp,avatar]);
	
	this.skill_tree_1 = skill_tree_1;
	this.skill_tree_2 = skill_tree_2;
	this.skill_tree_3 = skill_tree_3;
	
}


mage.prototype = new player;
fighter.prototype = new player;
archer.prototype = new player;
custom_class.prototype = new player;