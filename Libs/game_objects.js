//Game_objects class

function game_object(){
	this.distance;
	
	this.position = {
		position: new Array(0),
		
		set_position : function(x,y){
			this.position[0] = new Array(2);
			this.position[0][0] = x;
			this.position[0][1] = y;
			return 'success';
		},
	
		get_position : function(){
			return this.position;
		}	
	}
	

}
var game_objects = [];

game_object.prototype.npc = function(name, type, id, alignment){
		this.name = name;
		this.type = type;
		this.hostile = alignment;
		this.id = id;
		
		this.stats = {
			health: 0,
			basic_attack: 0,
			magic_attack: 0,
			mana: 0,
			
			get : function(){
				return this;
			},
			
			set : function(health,basic_attack,magic_attack,mana){
				this.health = health;
				this.basic_attack = basic_attack;
				this.magic_attack = magic_attack;
				this.mana = mana;
				return 'success';
			}
		};
		
		this.exp_to_give= {
			level: 1,
			modifier: 0,
			experiance: 0,
			
			get : function(){
				return this;
			},
			
			set : function(level, modifier, experiance){
				this.level = level;
				this.modifier = modifier;
				this.experiance = experiance;
				return 'success';
			},
			
			adjust_exp : function(player_level){
				if(player_level >  this.level){
					this.experinace = this.experiance / this.modifier;
					return this.experinace;
				} else if(player_level == this.level){
					return this.experinace;
				} else {
					this.experiance = this.experiance * this.modifier;
					return this.experiance;
				}
			}
		}
	} //end of npc object
	
game_object.prototype.dead = function(){
	this.is_dead = false;
	if(this.stats.health <= 0){
		return this.is_dead = true;
	}else {
		return this.is_dead = false;
	}
}
	
game_object.prototype.item = function(id, usable, type, cost, belongs_to, on_use){
	this.id = id;
	this.usable = usable;
	this.type = type;
	this.cost = cost;
	this.belongs_to = belongs_to;

	
	if(on_use){
		this.on_use = {
			restore_health: false,
			restore_mana: false,
			increase_stat:false,
			deal_damage:false,
			is_quest_item:false,
			amount: 0,
			
			set : function(res_h, res_m, increase_stat, dd, is_q_item,amount){
				this.restore_health = res_h;
				this.restore_mana = res_m;
				this.increase_stat = increase_stat;
				this.deal_damage = dd;
				this.is_quest_item = is_q_item;
				this.amount = amount;
			},
			
			get : function(){
				return this;
			},
			
			use_item : function(){
				if(this.is_quest_item){
					use_quest_item(this.is_quest_item);
				}
				else if(this.restore_health){
					game_objects[0].player.increase_life(this.amount);
				} else if(this.restore_mana){
					game_objects[0].increase_mana(amount);
				} else if(this.increase_stat || this.deal_damage){
					game_objects[0].use_item(this.id);
				}
			}
		}
	}
	
	this.stats = {
		durability: 0,
		base_damage: 0,
		attack_speed:0,
		range:0,
		
		get : function(){
			return this;
		},
		
		set : function(dura, base_d, attack_s_mod, range){
			this.durability = dura;
			this.base_damage = base_d;
			this.attack_speed = attack_s_mod;
			this.range = range;
			return 'success';
		}
	}
	
	this.is_magic = {
		is_magic: false,
		attack_speed_mod: 0,
		move_speed_mod: 0,
		base_damage_mod: 0,
		base_magic_d_mod:0,
		fire_damage:0,
		ice_damage:0,
		electric_damage:0,
		poison_damage:0,
		heath_mod:0,
		mana_mod:0,
		strength_mod:0,
		dex_mod:0,
		intel_mod:0,
		
		get : function(){
			return this;
		},
		
		set : function(is_magic, attack_s_mod, move_s_mod, base_d_mod, 
						base_md_mod, fire, ice, electric, poison, health_m, mana_m, strength_m, dex_m, intel_m){
			this.is_magic = is_magic;
			this.attack_speed_mod = attack_s_mod;
			this.move_speed_mod = move_s_mod;
			this.base_damage_mod = base_d_mod;
			this.base_magic_d_mod = base_md_mod;
			this.fire_damage = fire;
			this.ice_damage = ice;
			this.electric_damage = electric;
			this.poison_damage = poison;
			this.health_mod = health_m;
			this.mana_mod = mana_m;
			this.strength_mode = strength_m;
			this.dex_mod = dex_m;
			this.intel_mod = intel_m;
			return 'success';
		}
	}
}

game_object.prototype.player = function(player){
	this.player = player;
	this.id = 'current_player';
}

game_object.prototype.update_distance_to_player = function(){

	var player_pos = game_objects[0].position.get_position();
	var object_pos = this.position.get_position();
	this.distance = Math.sqrt(Math.pow((player_pos[0][0] - object_pos[0][0]),2) + Math.pow((player_pos[0][1] - object_pos[0][1]), 2));
	return Math.floor(this.distance);
	
}
	
game_object.prototype.FindById = function(id){
	for(var i = 0; i < game_objects.length; i++){
		if(game_objects[i].id == id){
			return game_objects[i];
			break;
		}
	}
}


function create_new_game_object(type){

	game_objects.push(new game_object);
	
	switch(type){
		case 'npc':
		{
			var temp = game_objects.pop();
			temp.npc('john','fighter','billy',true);
	 		temp.stats.set(100, 40,0, 20);
	 		game_objects.push(temp);
	 		break;
		}
		case 'item':
		{
			var temp = game_objects.pop();
			temp.item('sword_of_crazy',true,'melee_sword',1000, 'billy');
			temp.stats.set(100,40,10,1.5);
			game_objects.push(temp);
			break;
		}
		case 'player':
		{
			var temp = game_objects.pop();
			temp.player(current_player);
			
			if(game_objects.length <= 1){
				game_objects.push(temp);
				break;
			} else{
				game_objects.splice(0,0, temp);
				break;
			}
		}
	}
}

function update_game_objects(){
	
	for(var i = 1; i < game_objects.length; i++){
		game_objects[i].update_distance_to_player;
	}
}
