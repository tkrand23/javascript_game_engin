//Objects file for the javascript games libray
//Written by Ryan Earl
//Updated on 3/5/2012


/*player class 
 * @name sets the name for the player
 * @type sets the type of character that the player is accepts: fighter, mage, archer, rogue, cleric
 * @gender accepts male or female
 * @experiance the players experinace points
 * @avatar the picture to be used by the player: this still needs to be done!
 */
function player(name, type, gender, experiance, avatar){
	this.name = name;
	this.type = type;
	this.gender = gender;
	this.experiance = experiance;
	this.avatar = avatar;
	
	
	//stats object
	this.stats = {
		health:0,
		max_health: 0,
		dex: 0,
		strength: 0,
		mana: 0,
		max_mana: 0,
		intel: 0,
		movespeed: 0,
		attackspeed: 0,
		base_damge: 0,
		base_magic_d: 0,
		defense:0,
		range: 0,
		
		
		//returns the stats object
		//accessed by saying: player_object.stats.get() 
		get_stats : function(){
			return this;
		},
		
		/*sets the stats objects
		 *@stats an object literal
		 */
		set: function( stats ){
			this.health = stats.health;
			this.max_health = stats.max_health;
			this.dex = stats.dex;
			this.strength = stats.strength;
			this.mana = stats.mana;
			this.max_mana = stats.max_mana;
			this.intel = stats.intel;
			this.movespeed = stats.movespeed;
			this.attackspeed = stats.attackspeed;
			this.base_damage = stats.base_damage;
			this.base_magic_d = stats.base_magic_d;
			this.defense = stats.defense;
			this.range = stats.range;
			return 'success';
		} 
	}
	
	//level object
	this.level = {
		base_level_exp: 100,
		modifier: 1.3,
		current_level: 1,

		//function that levels up the player
		level_up: function(){
				this.current_level += 1;
				this.base_level_exp = this.base_level_exp * this.modifier;
				this.base_level_exp = Math.floor(this.base_level_exp);
				return this.current_level;
		}
	}
	
	this.inventory = {
		inventory_items: [],
		items_count: 0,
		equiped_items: [],
		items_equiped: 0,
	}
}
//
//@item is an object
player.prototype.add_inventory = function(item){
    var _item = game_objects[0].FindById(item);
	this.inventory.inventory_items.push(_item)
	_item.belongs_to = this.name;
	this.items_count++;
}
		
player.prototype.remove_inventory = function(item){
    var _item = game_objects[0].FindById(item);
	this.inventory.inventory_items.pop(_item);
	_item.belongs_to = '';
	this.items_count--;
}

player.prototype.equip_item = function(item){
	
	var _item = game_objects[0].FindById(item);
	
	if(_item.belongs_to != current_player.name){
		return;
	}
	
	this.inventory.equiped_items.push(_item);
	this.inventory.items_equiped++;

	current_player.modify_stats_for_equipment();
}

player.prototype.use_item = function(item){
	var _item = game_objects[0].FindById(item);
	
	if(_item.belongs_to!= current_player.name){
		return;
	}
		current_player.modify_stats_for_equipment(_item);
}

player.prototype.modify_stats_for_equipment = function(_item){
	var ind_item = '';
	for(var i = 0; i < this.inventory.equiped_items.length; i++){
		if(_item){
			ind_item = _item;
		} else {
			ind_item = this.inventory.equiped_items[i];
		}
		
		//normal stats
		this.stats.base_damage += ind_item.stats.base_damage;
		this.stats.attackspeed = ind_item.stats.attack_speed;
		this.stats.range = ind_item.stats.range;
		
		if(ind_item.is_magic.is_magic == true){
			//magic stats
			this.stats.base_damage += ind_item.is_magic.base_damage_mod;
			this.stats.base_damage += ind_item.is_magic.fire_damage; 
			this.stats.base_damage += ind_item.is_magic.ice_damage;
			this.stats.base_damage += ind_item.is_magic.electric_damage;
			this.stats.base_damage += ind_item.is_magic.poison_damage;
			this.stats.attackspeed += ind_item.is_magic.attack_speed_mod;
			this.stats.dex += ind_item.is_magic.dex_mod;
			this.stats.strength += ind_item.is_magic.strength_mod;
			this.stats.max_health += ind_item.is_magic.strength_mod;
			this.stats.intel += ind_item.is_magic.intel_mod;
			this.stats.max_mana += ind_item.is_magic.mana_mod;
			this.stats.movespeed += ind_item.is_magic.move_speed_mod;
		}
		if(_item){break;} 
	}
}

//adds the decrease life method to the player
//Takes a negitive number
//@decrement the amount to be subtracted from the players health
player.prototype.decrease_life = function(decrement){
	this.is_dead = false;

	if(decrement > 0){
		decrement = 0;
	}
	this.stats.health = this.stats.health + decrement;
		
	if(this.stats.health <= 0){
		return this.is_dead = true;
		this.stats.health= 0;
	} else{
		return this.stats.health;
	}
}

//adds the increase life method to the player
//Takes a positive number
//@increment the amount to be added to the players health
player.prototype.increase_life = function(increment){
	
	if(increment < 0){
		increment = 100;
	}
	this.stats.health += increment;
	
	if(this.stats.health > this.stats.max_health){
		this.stats.health = this.stats.max_health;
	}
	return this.stats.health;
}

//adds the increase mana method to the player
//takes a positive number
//@increment the amount to be added to the players health
player.prototype.increase_mana = function(increment){
	if(increment < 0){
		increment = 100;
	}

	this.stats.mana += increment;
	
	if(this.stats.mana > this.stats.max_mana){
		this.stats.mana = this.stats.max_mana;
	}
	return this.stats.mana;
}

//adds the decrease mana method to the player
//takes as negitive number
//@decrement the amount to subtract from the players mana
player.prototype.decrease_mana = function(decrement){
	if(decrement > 0){
		decrement = 0;
	}
	this.stats.mana += decrement;
	
	if(this.stats.mana <= 0){
		this.stats.mana = 0;
		return 'No Mana';
	}else{
		return this.stats.mana;
	}
}

//adds the melee attack method to the player
//@target the object being attacked	
player.prototype.me_attack = function(target){
	var tar = '';

	tar = game_objects[0].FindById(target);
	
	if(tar.distance > this.stats.range){
		return 'Target not in range!';
	}
		
	if(tar.hostile && !tar.dead()){
		tar.stats.health -= this.stats.base_damage;
		return tar.stats.health;
	} else if(tar.dead()){
		return tar.dead();
	}
	
}

//adds the magic attack method to the player
//@target the object being attacked
player.prototype.ma_attack = function(target){
	var tar = '';

	tar = game_objects[0].FindById(target);
	
	if(tar.distance > this.stats.range){
		return 'Target not in range!';
	}

	if(tar.hostile && !tar.dead()){
		tar.stats.health -= this.stats.base_magic_d;
		return tar.stats.health;
	} else if(tar.dead()){
		return tar.dead();
	}
}

//adds the offensive skill method to the player
//@target the object to be affected by the skill
//@skill the skill that the player is using
player.prototype.use_attack_skill = function(target, skill){
    var current_mana = this.stats.mana;
    var tar = game_objects[0].FindById(target);

    if(tar.hostile && !tar.dead()) {
        if(tar.distance != skill.range){
            return 'Target not in range!';
        }
        if(skill.cost > current_mana) {
            return 'Not enough mana!';
        } else {
            current_player.decrease_mana(skill.cost);
            tar.stats.health -= skill.damage;
            return tar.stats.health;
        }
    } else if(tar.dead()){
        return tar.dead();
    }
}

//adds the buff skill method to the player
//@skill the skill to be used to buff the player
player.prototype.use_buff_skill = function(skill){
    var current_mana = this.stats.mana;
    
    if(skill.cost > current_mana){
        return 'Not enough mana!';
    } else {
        current_player.decrease_mana(skill.cost);
        current_player.increase_stats_for_buff(skill);
        return skill.description;
    }
}

//adds the stats from a buff to the players stats
//@skill the skill being used to buff the player
player.prototype.increase_stats_for_buff = function(skill){
	this.stats.set(skill.buffs);
}

//function that creates the curruent player object
//@skill_1,skill_2,skill_3 these are all strings to be passed if the player is making a custom class
//they are names of skill trees from the other classes
function create_player(skill_1,skill_2,skill_3){
		var name = document.getElementById('name').value;
		var type = document.getElementById('type').value;
		var gender = document.getElementById('gender').value;
		var avatar = document.getElementById('avatar').value;

		
		switch( type ){
			case 'mage':
			{
				current_player =  new mage(name,type,gender,0,avatar);
				current_player.stats.set({health:100,max_health:100,dex:10,strength:10,mana:90,max_mana:90,intel:10,movespeed:5,attackspeed:6,base_damage:20,base_magic_d:15,defense:10,range:1});
				break;
			}
			case 'fighter':
			{
				current_player =  new fighter(name,type,gender,0,avatar);
				current_player.stats.set({health:100,max_health:100,dex:10,strength:10,mana:90,max_mana:90,intel:10,movespeed:5,attackspeed:6,base_damage:20,base_magic_d:15,defense:10,range:1});
				break;
			}
			case 'archer':
			{
				current_player =  new archer(name,type,gender,0,avatar);
				current_player.stats.set({health:100,max_health:100,dex:10,strength:10,mana:90,max_mana:90,intel:10,movespeed:5,attackspeed:6,base_damage:20,base_magic_d:15,defense:10,range:1});
				break;
			}
			case 'custom':
			{   var temp_mage = new mage();
			    var temp_fighter = new fighter();
			    var temp_archer = new archer();
			    var temp_skills = [];
			    var player_skills = [];
			    var j = 0;
                temp_skills[0] = temp_mage.fire_skills.get_skills();
                temp_skills[1] = temp_mage.ice_skills.get_skills();
                temp_skills[2] = temp_fighter.offensive_skills.get_skills();
                temp_skills[3] = temp_fighter.defensive_skills.get_skills();
                temp_skills[4] = temp_archer.elemental_skills.get_skills();
                temp_skills[5] = temp_archer.martial_skills.get_skills();
                
                for(var i = 0; i < 6; i++ ){
                    if(skill_1 == temp_skills[i].name){
                        player_skills[j] = temp_skills[i];
                        j++;
                    } else if(skill_2 == temp_skills[i].name){
                        player_skills[j] = temp_skills[i];
                        j++;
                    } else if(skill_3 == temp_skills[i].name){
                        player_skills[j] = temp_skills[i];
                        j++;
                    }
                }
                
				current_player = new custom_class(name,type,gender,0,avatar,player_skills[0],player_skills[1],player_skills[2]);
				current_player.stats.set({health:100,max_health:100,dex:10,strength:10,mana:90,max_mana:90,intel:10,movespeed:5,attackspeed:6,base_damage:20,base_magic_d:15,defense:10,range:1});

				delete temp_skills;
				delete player_skills;
				delete temp_mage;
				delete temp_fighter;
				delete temp_archer;
				break;
			}
		}
		
		current_player_stats = '';
		create_new_game_object('player'); //make the current player the first instance in the game_objects array
}

//update gets called once per frame
//@health_mod tells whether or not the player has taken or gained health
//@exp_mod tess wheather or not the player has gained experiance
//@focus the current object that the player has selected	
function update(health_mod, exp_mod, focus){
	current_player_stats = current_player.stats.get_stats();
	
	current_player.experiance += exp_mod;
	
	var current_health = null;
	
	if(health_mod < 0){
		current_health = current_player.decrease_life(health_mod);
	}
	else if(health_mod > 0){
		current_health = current_player.increase_life(health_mod);
	}
	
	if(current_player.experiance >= current_player.level.base_level_exp){
			current_player.level.level_up();
			current_player_stats.health = 100;
	}
	
	 var attack_this = current_player.me_attack(focus);
	 
	 if(attack_this === true){
	 	alert('Your target is dead!');
	 }
	 
    console.log(attack_this);
	
	console.log('current_experinace: '+current_player.experiance);
	console.log('current_base_experinace_needed_to_level: '+current_player.level.base_level_exp);
	console.log('current_level: '+current_player.level.current_level);
	console.log('current_health: '+current_health);
	
	if(current_health == true){
		alert('You are dead!');
	}
}
