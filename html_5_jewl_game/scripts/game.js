jewel.game = (function() {
    var dom = jewel.dom;
    $ = dom.$;
    
    function setup() {
    	//disable native touchmove behavior to prevent overscroll
    	dom.bind(document, "touchmove", function(event){
    		event.preventDefault();
    	});
    	
    	//hide the address bar on android devices
    	if(/Android/.test(navigator.userAgent)){
    		$("html")[0].style.height = "200%";
    		setTimeout(function(){
    			window.scrollTo(0,1);
    		}, 0);
    	}
    }
    
    function showScreen(screenId){
        var activeScreen = document.getElementsByClassName("screen active")[0];
        var screen = $("#" + screenId)[0];
		console.log(activeScreen, 'inside active screen');
        if(activeScreen){
            dom.removeClass(activeScreen, "active");
        }
        jewel.screens[screenId].run();
        dom.addClass(screen, "active");
        
    }
    return {
        showScreen: showScreen,
        setup : setup
    };
})();
